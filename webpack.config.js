'use strict';

const jsBaker = require('js-baker');

let config = {
    entry: {
        cyWizard: './src/js/wizard.js'
    }
};

module.exports = jsBaker.lib(config);
