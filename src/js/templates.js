'use strict';

const TPL_STEPS = ({currentStep, totalSteps}) => Array(totalSteps).fill('').map((val,i) => `<span class="step-item ${(i+1) === currentStep ? 'active' : ''}"></span>`).join('');

const TPL_CONTENT_AREA = ({content, currentStep, totalSteps}) => `
    <div class="cy-wizard">
        <div class="text-right">${TPL_STEPS({currentStep, totalSteps})}</div>

        <div data-container="content">${content}</div>
    </div>
`;

export {TPL_CONTENT_AREA,TPL_STEPS};
