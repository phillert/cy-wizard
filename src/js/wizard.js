'use strict';

import {TPL_CONTENT_AREA} from './templates';
import '../css/styles.scss';
class Wizard {

    constructor({el, init, destroy, steps = []}) {
        this._totalSteps = steps.length;
        this._currentStep = 1;
        this._init = init;
        this._destroy = destroy;
        this._el = document.querySelector(el);
        this._steps = steps;

        this._render();
        this._initController();

        if(this._init){
            this._init.call(this);
        }
    }

    /**
     * Add/update the HTML to the DOM container of the widget
     *
     * @private
     */
    _render() {
        this._el.innerHTML = TPL_CONTENT_AREA({
            content:this._steps[this._currentStep-1].template(),
            currentStep: this._currentStep,
            totalSteps: this._totalSteps
        });
    }

    /**
     * Initialize step controller
     *
     * @private
     */
    _initController() {
        this._steps[this._currentStep-1].controller();
    }

    // +++ Public Methods ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * Move to another step and render its template
     *
     * @param  {} step
     */
    goToStep(step) {
        if(step >= 0 && step <= this._totalSteps) {
            this._currentStep = step;
            this._render();
            this._initController();
        }
    }

    /**
     * Move to the next step and render its template
     */
    next() {
        this.goToStep(++this._currentStep);
    }

    /**
     * Calling the injected destroy method
     */
    destroy() {
        this._destroy.call(this);
    }
}

export default Wizard;
