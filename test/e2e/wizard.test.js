import Nightmare from 'nightmare';

const ROOTURL = `http://localhost:${process.env.E2E_PORT}/test/e2e/htdocs/`;
const GREEN = '#309630';

const nightmare = Nightmare();
nightmare.goto(ROOTURL);

test(`Test if widget jumps to step 2`, (done) => {
    let expectedText = 'step 2';
      nightmare
        .refresh()
        .click('button[data-action="next"]')
        .evaluate(() => document.querySelector('div[data-container="content"] > p').innerText)
        .then(text => {
          expect(text).toEqual(expectedText);
          done();
        });
    })

test(`Test if navigation dot get active for step 2`, (done) => {
  let expectedText = 'step-item active';
    nightmare
      .refresh()
      .click('button[data-action="next"]')
      .evaluate(() => document.querySelector(`.step-item:nth-child(2)`).className)
      .then(text => {
        expect(text).toEqual(expectedText);
        done();
      });
  })

test(`Test if widget jumps to step 3`, (done) => {
  let expectedText = 'step 3';
    nightmare
      .refresh()
      .click('button[data-action="next"]')
      .click('button[data-action="next"]')
      .evaluate(() => document.querySelector('div[data-container="content"] > p').innerText)
      .then(text => {
        expect(text).toEqual(expectedText);
        done();
      });
  })

test(`Test if navigation dot get active for step 3`, (done) => {
  let expectedClass = 'step-item active';
  nightmare
      .refresh()
      .click('button[data-action="next"]')
      .click('button[data-action="next"]')
      .evaluate(() => document.querySelector(`.step-item:nth-child(3)`).className)
      .then(value => {
        expect(value).toEqual(expectedClass);
        done();
      })
  })

  // test(`Test if finished step has green dot`, (done) => {
  //   let expectedColor = GREEN;
  //   nightmare
  //       .refresh()
  //       .click('button[data-action="next"]')
  //       .click('button[data-action="next"]')
  //       .evaluate(() => document.querySelector(`.step-item:nth-child(3)`).style.backgroundColor)
  //       .then(value => {
  //         expect(value).toEqual(expectedColor);
  //         done();
  //       })
  //   })
