# cy-wizard

Generic wizard for anything steppable.

Requirements: `cy-bootstrap-theme` (provides the wizard css)

## Develop

```
npm install
npm run js-baker watch-dev
```

and in another terminal session

```
python -m SimpleHTTPServer
```

## Wizard()

With Webpack/ES6

    import Wizard from 'cy-wizard';

    const config = {};
    new Wizard(config)

ES5

    <script src="path/to/dist/cyWizard.js"></script>

---------------------------------------------------------

    var config = {};
    new window.cyWizard.default(config)

...